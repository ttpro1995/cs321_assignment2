package lab2;


import java.io.*;
import java_cup.runtime.*;

%%

%public
%class Scanner
%implements sym

%line
%column

%cup
%cupdebug



%{
  StringBuffer string = new StringBuffer();
    int _yyline, _yycolumn;
  private Symbol symbol(int type) {
    return new MySymbol(type, yyline+1, yycolumn+1);
  }

  private Symbol symbol(int type, Object value) {
    return new MySymbol(type, yyline+1, yycolumn+1, value);
  } 
%}

/* main character classes */

LineTerminator = \r|\n|\r\n
WhiteSpace = {LineTerminator} | [ \t\f]
Comment = "//"[^\n]*[\n]
Comment2 = "/*" ~"*/"

Identifier = [A-Za-z_][A-Za-z0-9_]* /*first character cannot be number */

IntegerLiteral = 0 | [1-9][0-9]*
DoubleLiteral  = ([0-9]+\.?[0-9]*|[0-9]*\.?[0-9]+)(E[+-]?[0-9]+)? // ? = maybe appear
StringLiteral = \"([^\\\"\n]|\\.)*\" // start with ", contain anything not \ along
%%

<YYINITIAL> {
  {Comment}                   { /* ignore */ }
  {Comment2}                  { /* ignore */ }
       "/*" {
Errors.fatal(yyline+1, yycolumn+1, "No close comment") ; 
System.exit(-1);
}
  /* keywords */
  "int"                          { return symbol(INT); }
  "return"                       { return symbol(RETURN); }
  "void"                         { return symbol(VOID); }   
  "if"                           { return symbol(IF); }   
  "else"                         { return symbol(ELSE); }   
  "float"                         { return symbol(FLOAT);}
  "const"                        { return symbol(CONST);}

  
  /* punctuators */
  "("                            { return symbol(LPAREN); }
  ")"                            { return symbol(RPAREN); }
  "{"                            { return symbol(LCURLY); }
  "}"                            { return symbol(RCURLY); }
  "["                            { return symbol(LSQBRACKET); }
  "]"                            { return symbol(RSQBRACKET); }
  ";"                            { return symbol(SEMICOLON); }
  ","                            { return symbol(COMMA); }

  "<"                            { return symbol(LESS); }
  ">"                            { return symbol(GREATER); }
  "+"                            { return symbol(PLUS); }
  "-"                            { return symbol(MINUS); }
  "/"                            { return symbol(DIVIDE); }
  "*"                            { return symbol(TIMES); }
  "="                            { return symbol(ASSIGN); }
  "=="                           { return symbol(EQUALS); }
   "&"                            { return symbol(ADDROF); }

   


 {IntegerLiteral}               {
	  int val;
	   try {
	     val = (new Integer(yytext())).intValue();
	   } catch (NumberFormatException e) {
	     Errors.warn(yyline+1, yycolumn+1, " a really big integer literal ");
	     val = Integer.MAX_VALUE;
	   } 
	  return symbol(INTLITERAL, val); 
  }
  {DoubleLiteral}               { return symbol(FLOATING_POINT_LITERAL, new Double(yytext())); }
  {StringLiteral}                { return symbol(STRINGLITERAL, yytext()); }
  \"([^\\\"\n]|\\.)*   { 
Errors.fatal(yyline+1, yycolumn+1, "Unterminated string literal") ; 
System.exit(-1);
}

  {WhiteSpace}                   { /* ignore */ }
  



  /* identifiers */ 
  {Identifier}                   { return symbol(ID, yytext()); }  
}



.                              { Errors.fatal(yyline+1, yycolumn+1, "Illegal character \"" + yytext()+ "\""); 
       System.exit(-1); } 
<<EOF>>                          { return symbol(EOF); }
.  { Errors.fatal(yyline+1, yycolumn+1, "Illegal character \"" + yytext()+ "\""); 
       System.exit(-1); }   


